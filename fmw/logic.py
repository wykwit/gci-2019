import gnupg
import hashlib
import os
import subprocess
import wget

gpg = gnupg.GPG()

def download(url):
	r = wget.download(url)
	print()
	return r

def file_exists(path):
	return os.path.isfile(path)

def file_delete(path):
	return os.unlink(path)

def gpg_import(key):
	with open(key, "r") as f:
		r = gpg.import_keys(f.read())
	return r

def gpg_verify(fname):
	with open(fname, "rb") as f:
		r = gpg.verify_file(f)
	return r

def hash_verify(iso, checksum):
	with open(checksum, "r") as f:
		c = f.read().replace(" ", "").split()
		s = [x for x in c if x.find(iso) != -1 and x.find("#") == -1]
	if not s: return False
	s = s[0].split("=")[-1]
	with open(iso, "rb") as f:
		x = hashlib.sha256(f.read()).hexdigest()
	return (s == x)

def removable_devices():
	r = []
	for x in os.listdir("/sys/block"):
		with open("/sys/block/"+x+"/removable", "r") as f:
			if int(f.read()) == 1:
				r.append(x)
	r = ["/dev/"+x for x in r]
	return r

def device_vendor(device):
	x = device.split("/")[-1]
	with open("/sys/block/"+x+"/device/vendor", "r") as f:
		r = f.read().strip()
	return r

def burn(iso, drive):
	return subprocess.run(["sudo", "dd", "if="+iso, "of="+drive, "status=progress"])

