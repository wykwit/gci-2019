version = "31"
minor = "1.9"

variants = ["Workstation", "Spin", "Lab"]
spins = ["KDE", "Xfce", "LXQt", "MATE_Compiz", "Cinnamon", "LXDE", "SoaS"]
labs = ["Astronomy", "Design Suite", "Python Classroom", "Security Lab", "Robotics Suite"]

workstation_download = "https://download.fedoraproject.org/pub/fedora/linux/releases/"+version+"/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-"+version+"-"+minor+".iso"
workstation_checksum = "https://getfedora.org/static/checksums/Fedora-Workstation-"+version+"-"+minor+"-x86_64-CHECKSUM"

spins_baseurl = "https://download.fedoraproject.org/pub/fedora/linux/releases/"+version+"/Spins/x86_64/iso/Fedora-FLAVOUR-Live-x86_64-"+version+"-"+minor+".iso"
spins_download = dict()
for spin in spins:
	spins_download[spin] = spins_baseurl.replace("FLAVOUR", spin)
spins_checksum = "https://spins.fedoraproject.org/static/checksums/Fedora-Spins-"+version+"-"+minor+"-x86_64-CHECKSUM"

labs_baseurl = "https://download.fedoraproject.org/pub/alt/releases/"+version+"/Labs/x86_64/iso/Fedora-FLAVOUR-Live-x86_64-"+version+"-"+minor+".iso"
labs_download = {
	labs[0]: labs_baseurl.replace("FLAVOUR", "Astronomy_KDE"),
	labs[1]: labs_baseurl.replace("FLAVOUR", "Design_suite"),
	labs[2]: labs_baseurl.replace("FLAVOUR", "Python-Classroom"),
	labs[3]: labs_baseurl.replace("FLAVOUR", "Security"),
	labs[4]: labs_baseurl.replace("FLAVOUR", "Robotics"),
}
labs_checksum = "https://labs.fedoraproject.org/static/checksums/Fedora-Labs-"+version+"-"+minor+"-x86_64-CHECKSUM"

gpg_download = "https://getfedora.org/static/fedora.gpg"
