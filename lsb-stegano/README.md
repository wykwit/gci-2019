Google Code-in Task: create a script that encodes and decodes a text from an image.

This tool uses least significant bits of each pixel to hide data in losslessly compressed images (png).

```
usage: stegano.py [-h] [-d] -i INPUT_FILE [-o OUTPUT_FILE] [-m MESSAGE] [-b BITS]

Perform LSB steganography.

optional arguments:
  -h, --help      show this help message and exit
  -d              Decrypt instead of encrypting.
  -i INPUT_FILE
  -o OUTPUT_FILE
  -m MESSAGE      Message that will be encrypted.
  -b BITS         Amount of bits on each pixel used for decryption.
```

### Examples:
#### A simple message
##### Encrypt:
`./stegano.py -i fedora_logo.png -m "This is a test." -o msg.png`
##### Decrypt:
`./stegano.py -i msg.png -d -b 1`
#### A file
The file must be smaller than the image. You can write at most (img.height * img.width * 3) bits to the image. ( *3, because we typically have three color bands in the image)
##### Encrypt:
`base64 test.mp3 | ./stegano.py -i fedora_logo.png -o music.png`
If more than 1 bit was used to encrypt the image, you must specify the amount of bits used when you attempt to decrypt it.
##### Decrypt:
```
./stegano.py -i music.png -d -o mp3.base64
base64 -d mp3.base64 > hidden.mp3
```
#### Overwriting the image
##### Encrypt:
`./stegano.py -i overwrite.png -o overwrite.png -m "This image was overwritten."`
##### Decrypt:
`./stegano.py -i overwrite.png -d`
#### this program as the hidden message
##### Encrypt:
`./stegano.py -i fedora_logo.png -o stegano.png < stegano.py`
##### Decrypt:
`./stegano.py -i stegano.png -d -o still_stegano.py`

You can see files involved in the example_files directory.
