#include <stdio.h>
#include <string.h>

#define FILENAME_LEN 128
#define ITOA_LEN 22

char* delim;
char* output_filename = "xx";

void compose_filename(char* output_file, int i) {
	strcpy(output_file, output_filename);
	char str_i[ITOA_LEN] = "";
	sprintf(str_i, "%d", i);
	strcat(output_file, str_i);
}

int isdelim(char c) {
	for(int i = 0; delim[i] != '\0'; i++)
		if (c == delim[i]) return 1;
	return 0;
}

void help(void) {
	printf("\nusage: ./split [delim] [output_filename] [input_file]\n\n\t[delim] - the input will be split on any of the given characters\n\t[output_filename] - (default: output-) - output filename prefix\n\t[input_filename] - (default: stdin) - the input file\n\n");
}

int main(int argc, char* argv[]) {
	FILE* input_file = stdin;

	switch (argc) {
		case 4:	input_file = fopen(argv[3], "r");
		case 3:	output_filename = argv[2];
		case 2:	delim = argv[1]; break;
		case 1:	help(); return 0;
	}

	char output_file[FILENAME_LEN] = "";
	FILE *f; int i = 0;
	compose_filename(output_file, i);
	f = fopen(output_file, "a");

	char c;
	while (fscanf(input_file, "%c", &c) != EOF) {
		if (isdelim(c)) {
			fclose(f);
			compose_filename(output_file, ++i);
			f = fopen(output_file, "a");
		} else fprintf(f, "%c", c);
	}
	fclose(f);

	if(input_file != stdin) fclose(input_file);
	return 0;
}