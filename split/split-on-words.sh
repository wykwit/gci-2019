#!/bin/bash

delimiter=$1
if [ "$delimiter" == "" ]; then
	read -p "Delimiter: " delimiter
	echo "Reading input from stdin..."
fi
len=${#delimiter}
prefix="xx"
id=0
str=""
while read -rN1 c
do
	str="$str$c"
	if [ "${str:${#str}-$len}" == "$delimiter" ]
	then
		echo "$str" >> "$prefix$id"
		id=$(( $id + 1 ))
		str=""
	fi
done
if [ "$str" != "" ]
then
	echo "$str" >> "$prefix$id"
fi