# split

Google Code-in Task: write a shell script to split files based on certain characters

Usage:
```./split [c] < [input_file]```
where `[c]` is a word consisting of characters we are spliting our `[input_file]` on

