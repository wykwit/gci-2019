Google Code-in Task: Build a python application and make a Flatpak package. Write down the steps in a Markdown file.

# Flatpak

## resources
- [@loganasherjones blog](https://www.loganasherjones.com/2018/05/using-flatpak-with-python/)
- [flatpak docs](http://docs.flatpak.org/en/latest/index.html)

## introduction
Flatpak is a software package manager, which offers a sandboxed environment. Programs distributed with flatpak are reffered to as "apps", because they run in a sandbox (isolated from OS and each other), similarly to apps on mobile systems. In addition, you don't need root permissions to use flatpak and install new apps. Flatpak is not intended to replace distro-level package managers like apt, dnf or pacman. It simply provides an alternative way to install end-user applications. For example: you cannot manage your drivers with flatpak, but you can use it to install GIMP or VLC.

## hello-world
To get familiar with flatpak, let's first make a hello-world package. The tutorial is available in [flatpak docs](http://docs.flatpak.org/en/latest/first-build.html).
Apps need a manifest file to build a package.
We can build to our local repo with a following command:
`flatpak-builder --repo=repo --force-clean build-dir org.flatpak.Hello.json`
> There's one thing docs don't mention, that could be frustrtating for beginners: when you use this command to make your own local repo, the path to "repo" folder will be urlencoded. That means, for example, whenever you have a space character appear in your path, it will be encoded as "%20". When you try to install anything from such a repo, flatpak might fail while trying to access a folder that doesn't exist. The simplest workaround is to use url friendly paths or symlink your directories accordingly.

Then we can install apps from our local repo with:
`flatpak --user install tutorial-repo org.flatpak.Hello`
and once the apps are installed, for future iterations we can simply update:
`flatpack --user update`
We would run our app with:
`flatpak run org.flatpak.Hello`

## explore-sdk
Let's see what is available in our SDK. It includes python binary, but we don't know what version yet. [Documentation](https://docs.flatpak.org/en/latest/available-runtimes.html) suggests that we take a look around.
We will change our manifest file and build a new app.
But first, let's take a repo folder outside of our project directory.
```
flatpak --user remote-delete tutorial-repo
mv hello-world/repo .
flatpak --user remote-add --no-gpg-verify local-repo repo
```
You can see how I changed the manifest file in this repo's "explore-sdk" folder.
Let's install the app.
```
cd explore-sdk
flatpak-builder --repo=../repo --force-clean build-dir org.flatpak.Explore.json 
flatpak --user install local-repo org.flatpak.Explore
```
Now we can get a bash session:
`flatpak run org.flatpak.Explore`
Apparently, there's no python2 and no python alias.
`python3 --version` yields "Python 3.7.4". We also have pip3 available.

## hello-python
We can proceed to write a regular python program, with the assumption that our code will run on python 3.7.
Let's make a simple program that will display our public IP. This will demonstrate how to manage python dependencies with flatpak's manifest file. 
If your program is structured as a proper python module, you may simply use flatpak-pip-generator, as described in the [documentation](http://docs.flatpak.org/en/latest/python.html).
We are only running a simple script, so we'll run flatpak-pip-generator for requests - our dependency.
Remember we are running in a sandbox, so we have to explicitly state what our app will need from the system. In this case: network connection.
Go ahead and read the content of "hello-python" subdirectory.
We build and install like before:
```
cd hello-python
flatpak-builder --repo=../repo --force-clean build-dir org.flatpak.HelloPython.json
flatpak --user install local-repo org.flatpack.HelloPython
flatpak run org.flatpak.HelloPython
```

Voilà, everything works fine!
